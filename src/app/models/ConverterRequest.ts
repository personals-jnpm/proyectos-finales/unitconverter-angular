export class ConverterRequest {
  private numericValue: string;
  private basePower: string;

  constructor(numericValue: string, basePower: string) {
    this.numericValue = numericValue;
    this.basePower = basePower;
  }
}
