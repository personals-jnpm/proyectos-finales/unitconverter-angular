export class ConverterReqGen {
  public numericValue: string;
  public originBasePower: string;
  public finalBasePower: string;

  constructor(numericValue: string, originBasePower: string, finalBasePower: string) {
    this.numericValue = numericValue;
    this.originBasePower = originBasePower;
    this.finalBasePower = finalBasePower;
  }
}
