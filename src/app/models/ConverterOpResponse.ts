export class ConverterOpResponse {
  public numericValueOne: string;
  public numericValueTwo: string;
  public resultValue: string;
  public process: string[];

  constructor(numericValueOne: string, numericValueTwo: string, resultValue: string, process: string[]) {
    this.numericValueOne = numericValueOne;
    this.numericValueTwo = numericValueTwo;
    this.resultValue = resultValue;
    this.process = process;
  }
}
