export class ConverterResponse {
  public numericValue: string;
  public resultValue: string;
  public process: string[];

  constructor(numericValue: string, resultValue: string, process: string[]) {
    this.numericValue = numericValue;
    this.resultValue = resultValue;
    this.process = process;
  }
}
