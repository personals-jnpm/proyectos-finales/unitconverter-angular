export class ConverterOpRequest {
  public numericValueOne: string;
  public numericValueTwo: string;
  public typeOperation: string;

  constructor(numericValueOne: string, numericValueTwo: string, typeOperation: string) {
    this.numericValueOne = numericValueOne;
    this.numericValueTwo = numericValueTwo;
    this.typeOperation = typeOperation;
  }
}
