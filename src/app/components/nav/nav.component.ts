import { Component, OnInit } from '@angular/core';
import {CONSTANTS_WELCOME} from "../../constants/constants";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  public constants = CONSTANTS_WELCOME;

  constructor() { }

  ngOnInit(): void {
  }

}
