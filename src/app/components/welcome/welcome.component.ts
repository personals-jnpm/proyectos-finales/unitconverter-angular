import {Component, OnInit} from '@angular/core';
import {CONSTANTS_WELCOME} from "../../constants/constants";

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  public constants = CONSTANTS_WELCOME;

  constructor() {
  }

  ngOnInit(): void {
  }

}
