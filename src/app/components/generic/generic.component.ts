import {Component, OnInit} from '@angular/core';
import {CONSTANTS_CALCULATOR} from "../../constants/constants";
import {NgForm} from "@angular/forms";
import {ConverterService} from "../../services/converter.service";
import {ConverterReqGen} from "../../models/ConverterReqGen";

@Component({
  selector: 'app-generic',
  templateUrl: './generic.component.html',
  styleUrls: ['./generic.component.css']
})
export class GenericComponent implements OnInit {

  public constants;
  public originBasePower: string;
  public numericValue: string;
  public finalBasePower: string;
  public resultValue: string;

  public errors: boolean;
  public isLoading: boolean;
  public errorMessages: string[];

  constructor(private converterService: ConverterService) {
    this.constants = CONSTANTS_CALCULATOR;
    this.originBasePower = "";
    this.numericValue = "";
    this.finalBasePower = "";
    this.resultValue = "";
    this.errors = false;
    this.isLoading = false;
    this.errorMessages = [];
  }

  ngOnInit(): void {
  }

  public clean(form: NgForm) {
    form.form.reset();
  }

  public clearError(): void {
    this.errors = false;
    this.resultValue = "";
  }

  public convert(): void {
    this.isLoading = true;
    this.converterService.genericConverter(new ConverterReqGen(this.numericValue, this.originBasePower,
      this.finalBasePower)).subscribe({
      error: err => {
        this.errorMessages = err.error.messages;
        this.errors = true;
        this.isLoading = false;
      },
      next: response => {
        this.resultValue = response.resultValue;
      },
      complete: () => {
        this.isLoading = false;
      }
    });
  }
}
