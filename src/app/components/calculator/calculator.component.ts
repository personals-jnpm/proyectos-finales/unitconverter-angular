import {Component, OnInit} from '@angular/core';
import {CONSTANTS_CALCULATOR} from "../../constants/constants";
import {Converter} from "../../types/converter";
import {CONVERTERS_ARRAY} from "../../constants/converters";
import {ConverterService} from "../../services/converter.service";
import {ConverterRequest} from "../../models/ConverterRequest";
import {finalize} from "rxjs";
import {ConverterOpRequest} from "../../models/ConverterOpRequest";

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

  public constants;
  public currentConverter: number;
  public converter: Converter;

  public firstValue: string;
  public secondValue: string;
  public resultValue: string;
  public process: string[];

  public isLoading: boolean;
  public binaryOp: boolean;
  public isProcess: boolean;
  public errors: boolean;
  public errorMessage: string;

  constructor(private converterService: ConverterService) {
    this.constants = CONSTANTS_CALCULATOR;
    this.currentConverter = 0;
    this.converter = this.getDefaultConverter();
    this.firstValue = '';
    this.secondValue = '';
    this.resultValue = '';
    this.process = [];
    this.isLoading = false;
    this.binaryOp = false;
    this.isProcess = false;
    this.errors = false;
    this.errorMessage = '';
  }

  ngOnInit(): void {
  }

  public getDefaultConverter(): Converter {
    return CONVERTERS_ARRAY[0];
  }

  public setConverter(option: number): void {
    this.currentConverter = option;
    this.converter = CONVERTERS_ARRAY[option];
    if ((option == 3 || option == 4 || option == 5) && !this.binaryOp
      || option != 3 && option != 4 && option != 5 && this.binaryOp) {
      this.changeConverterType();
    }
    this.clearFields();
  }

  public changeUnits(): void {
    let temporal: string = this.converter.firstValueLabel;
    this.converter.firstValueLabel = this.converter.secondValueLabel;
    this.converter.secondValueLabel = temporal;
    this.clearFields();
  }

  public changeConverterType(): void {
    this.binaryOp = !this.binaryOp;
  }

  public clearFields(): void {
    this.firstValue = '';
    this.secondValue = '';
    this.resultValue = '';
    this.errors = false;
    this.isProcess = false;
    this.process = [];
  }

  public clearError(): void {
    this.errors = false;
  }

  public convert(): void {
    if (this.converter.firstValueLabel == this.constants.BINARY) {
      this.toDecimal(this.constants.BASE_POWER_BINARY);
    } else if (this.converter.firstValueLabel == this.constants.OCTAL) {
      this.toDecimal(this.constants.BASE_POWER_OCTAL);
    } else if (this.converter.firstValueLabel == this.constants.HEXADECIMAL) {
      this.toDecimal(this.constants.BASE_POWER_HEXADECIMAL);
    } else if (this.converter.firstValueLabel == this.constants.DECIMAL &&
      this.converter.secondValueLabel == this.constants.BINARY) {
      this.decimalTo(this.constants.BASE_POWER_BINARY);
    } else if (this.converter.firstValueLabel == this.constants.DECIMAL &&
      this.converter.secondValueLabel == this.constants.OCTAL) {
      this.decimalTo(this.constants.BASE_POWER_OCTAL);
    } else if (this.converter.firstValueLabel == this.constants.DECIMAL &&
      this.converter.secondValueLabel == this.constants.HEXADECIMAL) {
      this.decimalTo(this.constants.BASE_POWER_HEXADECIMAL);
    } else if (this.converter.firstValueLabel == this.constants.FIRST_SUM_NUMBER) {
      this.sumBinary(this.constants.SUM_TYPE);
    } else if (this.converter.firstValueLabel == this.constants.MINUEND) {
      this.sumBinary(this.constants.SUBTRACTION_TYPE);
    } else if (this.converter.firstValueLabel == this.constants.FIRST_MULTIPLY_NUMBER) {
      this.multiplyBinary(this.constants.MULTIPLY_TYPE);
    }
  }

  public toDecimal(basePower: string) {
    this.isLoading = true;
    this.converterService.convertToDecimal(new ConverterRequest(this.firstValue, basePower))
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe({
        error: error => {
          this.errorMessage = error.error.messages[0];
          this.errors = true;
        },
        next: response => {
          this.process = response.process;
          this.isProcess = true;
          this.resultValue = response.resultValue;
        }
      });
  }

  public decimalTo(basePower: string) {
    this.isLoading = true;
    this.converterService.convertDecimalTo(new ConverterRequest(this.firstValue, basePower))
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe({
        error: error => {
          this.errorMessage = error.error.messages[0];
          this.errors = true;
        },
        next: response => {
          this.process = response.process;
          this.isProcess = true;
          this.resultValue = response.resultValue;
        }
      });
  }

  public sumBinary(typeOperation: string) {
    this.isLoading = true;
    this.converterService.sum(new ConverterOpRequest(this.firstValue, this.secondValue, typeOperation))
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe({
        error: error => {
          this.errorMessage = error.error.messages[0];
          this.errors = true;
        },
        next: response => {
          this.process = response.process;
          this.isProcess = true;
          this.resultValue = response.resultValue;
        }
      });
  }

  public multiplyBinary(typeOperation: string) {
    this.isLoading = true;
    this.converterService.multiply(new ConverterOpRequest(this.firstValue, this.secondValue, typeOperation))
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe({
        error: error => {
          this.errorMessage = error.error.messages[0];
          this.errors = true;
        },
        next: response => {
          this.process = response.process;
          this.isProcess = true;
          this.resultValue = response.resultValue;
        }
      });
  }
}
