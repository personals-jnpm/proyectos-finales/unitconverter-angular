import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConverterRequest} from "../models/ConverterRequest";
import {ConverterResponse} from "../models/ConverterResponse";
import {Observable} from "rxjs";
import {ConverterOpRequest} from "../models/ConverterOpRequest";
import {ConverterOpResponse} from "../models/ConverterOpResponse";
import {ConverterReqGen} from "../models/ConverterReqGen";

@Injectable({
  providedIn: 'root'
})
export class ConverterService {

  public baseUrl: string = 'https://unit-converter-usta.herokuapp.com/api/';

  constructor(private http: HttpClient) {
  }

  public convertToDecimal(model: ConverterRequest): Observable<ConverterResponse> {
    return this.http.post<ConverterResponse>(this.baseUrl + 'to-decimal', model);
  }

  public convertDecimalTo(model: ConverterRequest): Observable<ConverterResponse> {
    return this.http.post<ConverterResponse>(this.baseUrl + 'decimal-to', model);
  }

  public sum(model: ConverterOpRequest): Observable<ConverterOpResponse> {
    return this.http.post<ConverterOpResponse>(this.baseUrl + 'sum-binary', model);
  }

  public multiply(model: ConverterOpRequest): Observable<ConverterOpResponse> {
    return this.http.post<ConverterOpResponse>(this.baseUrl + 'multiply-binary', model);
  }

  public genericConverter(model: ConverterReqGen): Observable<ConverterResponse> {
    return this.http.post<ConverterResponse>(this.baseUrl + 'generic-converter', model);
  }
}
