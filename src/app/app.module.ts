import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { FooterComponent } from './components/footer/footer.component';
import {FormsModule} from "@angular/forms";
import {ToastrModule} from "ngx-toastr";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MathjaxModule} from "mathjax-angular";
import { WelcomeComponent } from './components/welcome/welcome.component';
import { CalculatorComponent } from './components/calculator/calculator.component';
import {HttpClientModule} from "@angular/common/http";
import { AboutComponent } from './components/about/about.component';
import { ContentComponent } from './components/content/content.component';
import { GenericComponent } from './components/generic/generic.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    WelcomeComponent,
    CalculatorComponent,
    AboutComponent,
    ContentComponent,
    GenericComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MathjaxModule.forRoot(),
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
