import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AboutComponent} from "./components/about/about.component";
import {ContentComponent} from "./components/content/content.component";
import {GenericComponent} from "./components/generic/generic.component";

const routes: Routes = [
  {path: 'about', component: AboutComponent},
  {path: 'generic-converter', component: GenericComponent},
  {path: '', component: ContentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
