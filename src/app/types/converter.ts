export interface Converter  {
  titleOperation: string;
  firstValueLabel: string;
  secondValueLabel: string;
}
