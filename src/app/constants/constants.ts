export const CONSTANTS_WELCOME = {
  TITLE_APP: 'Unit Converter',
  MESSAGE_WELCOME: 'Esta aplicación permite realizar la conversión de diferentes sistemas numéricos como el binario,' +
    ' octal, hexadecimal al sistema decimal y viceversa.',
  BUTTON_LABEL_WELCOME: 'Convertir Ahora',
  TITLE_SECTION: 'Website Project',
  MESSAGE_SECTION: 'Además de conocer el resultado de la operación también podrás ver el procedimiento' +
    ' de conversión para brindarte una mayor comprensión sobre el tema siguiendo el paso a paso',
}

export const CONSTANTS_CALCULATOR = {
  BINARY: 'Binario',
  HEXADECIMAL: 'Hexadecimal',
  OCTAL: 'Octal',
  DECIMAL: 'Decimal',
  SUM_BINARY: 'Suma binaria',
  SUBTRACTION_BINARY: 'Resta binaria',
  MULTIPLY_BINARY: 'Multiplicación binaria',
  FIRST_SUM_NUMBER: 'Primer sumando',
  SECOND_SUM_NUMBER: 'Segundo sumando',
  MINUEND: 'Minuendo',
  SUBTRACTING: 'Sustraendo',
  FIRST_MULTIPLY_NUMBER: 'Primer factor',
  SECOND_MULTIPLY_NUMBER: 'Segundo factor',
  RESULT_VALUE: 'Resultado',
  CONVERT_BUTTON: 'Convertir',
  OPERATION_BUTTON: 'Calcular',
  LOADING_BUTTON: 'Cargando...',
  PROCESS: 'Procedimiento',
  BASE_POWER_BINARY: '2',
  BASE_POWER_OCTAL: '8',
  BASE_POWER_HEXADECIMAL: '16',
  BASE_POWER_DECIMAL: '10',
  SUM_TYPE: 'suma',
  SUBTRACTION_TYPE: 'resta',
  MULTIPLY_TYPE: 'multiply',

  GENERIC_CONVERTOR_TITLE: 'Convertidor de sistemas numéricos genérico',
  ORIGIN_BASE_POWER: 'Base numérica de orígen',
  NUMERIC_VALUE: 'Valor numérico',
  FINAL_BASE_POWER: 'Base numérica de destino',
  CLEAN_BUTTON: 'Limpiar',
  VIEW_PROCESS: 'Ver procedimiento'
}
