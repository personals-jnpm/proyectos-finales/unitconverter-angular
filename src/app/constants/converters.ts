import {CONSTANTS_CALCULATOR as constants} from "./constants";

export const CONVERTERS_ARRAY = [
  {
    titleOperation: 'Binary Converter',
    firstValueLabel: constants.BINARY,
    secondValueLabel: constants.DECIMAL
  },
  {
    titleOperation: 'Hexadecimal Converter',
    firstValueLabel: constants.HEXADECIMAL,
    secondValueLabel: constants.DECIMAL
  },
  {
    titleOperation: 'Octal Converter',
    firstValueLabel: constants.OCTAL,
    secondValueLabel: constants.DECIMAL
  },
  {
    titleOperation: 'Sum Binary',
    firstValueLabel: constants.FIRST_SUM_NUMBER,
    secondValueLabel: constants.SECOND_SUM_NUMBER
  },
  {
    titleOperation: 'Subtraction Binary',
    firstValueLabel: constants.MINUEND,
    secondValueLabel: constants.SUBTRACTING
  },
  {
    titleOperation: 'Multiply Binary',
    firstValueLabel: constants.FIRST_MULTIPLY_NUMBER,
    secondValueLabel:  constants.SECOND_MULTIPLY_NUMBER
  },
]
